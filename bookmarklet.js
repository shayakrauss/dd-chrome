javascript:(function(){
  var pattern = new RegExp("(https:\/\/www.amazon.com\/).*([dg]p\/)(?:product\/)?(..........).*|(https:\/\/smile.amazon.com\/).*([dg]p\/)(?:product\/)?(..........).*", "i"),
      match = location.href.match(pattern),
      image = document.getElementById('landingImage').src;
  var price = "";
  if(document.getElementById('priceblock_dealprice') !== null) {
    price = " [b](" + document.getElementById('priceblock_dealprice').innerHTML + ")[/b]";
  } else if(document.getElementById('priceblock_ourprice') !== null) {
    price = " [b](" + document.getElementById('priceblock_ourprice').innerHTML + ")[/b]";
  }
  var URL = (match[1] !== undefined) ? match[1] + "dp/" + match[3] +"/?tag=cl03f-20&smid=ATVPDKIKX0DER": match[4] + "dp/" + match[6] +"/?tag=cl03f-20&smid=ATVPDKIKX0DER";
  var result = "[url="+URL+"]"+document.title + price + "[/url][br][br][url="+URL+"][img]"+image+"[/img][/url]";
  if(document.execCommand('copy') !== false) {
    var ddLink = document.createElement('span'),
        range = document.createRange();
    ddLink.id = "ddLink";
    ddLink.innerText = result;
    document.body.appendChild(ddLink);
    range.selectNode(document.getElementById('ddLink'));
    window.getSelection().addRange(range);
    document.execCommand('copy');
    document.getElementById('ddLink').remove();
    setTimeout(function() {
      alert('DD link copied to clipboard...')
    }, 1000);
  } else {
    window.prompt("Press control-c",result);
  } void(0);
})()
