let curUrl = location.href;

chrome.storage.sync.get({
  autoRedirect: 'autoRedirect',
  redirectToSmile: 'redirectToSmile'
}, function(obj) {
  let url = curUrl;
  if(obj.autoRedirect === true) {
    if(getQueryVariable('tag') === false || getQueryVariable('tag') !== 'cl03f-20') {
      url = updateQueryStringParameter(document.location.toString(), 'tag', 'cl03f-20');
    }
  }

  if(obj.redirectToSmile === true) {
    if(window.location.host === 'www.amazon.com') {
      url = url.replace('www.amazon.com', 'smile.amazon.com');
    }
  }

  if(curUrl !== url) {
    document.location.replace(url);
  }
});

window.onload = function() {
  setInterval(checkUrlChange, 1000);
  createButton();
};

function createButton() {
  let linkButton = document.createElement('button'),
      priceParent = document.getElementById('snsPrice'),
      priceDiv = document.getElementById('apex_desktop');
  linkButton.setAttribute('id', 'ddLinkButton');
  linkButton.innerHTML = 'Click to copy DDF friendly link to your clipboard';
  linkButton.onclick = copyDDtext;

  // priceDiv.appendChild(linkButton);
  if(priceParent == null) {
    priceParent = document.getElementById('price');
  }
  if(priceParent == null) {
    priceParent = priceDiv.appendChild(linkButton);
  }
  if(priceParent !== null && document.getElementById('ddLinkButton') === null) {
    priceParent.appendChild(linkButton);
  }
}

function copyDDtext() {
  let title = document.getElementById('productTitle').innerHTML.trim(),
      asin = document.getElementById('ASIN').value,
      protocol = document.location.protocol,
      host = document.location.host,
      price = '',
      image = document.getElementById('landingImage'),
      success = true,
      range = document.createRange(),
      selection,
      linkText = document.createElement('span'),
      url,
      result;

  if(image !== null) {
    image = "[img]" + image.src + "[/img]";
  } else {
    if(document.getElementsByClassName('image item selected')[0] === undefined) {
      let imageEl = document.getElementById('heroImage');
      let style = window.getComputedStyle(imageEl);
      image = "[img]" + style.backgroundImage.slice(4, -1).replace(/"/g, "") + "[/img]";
    } else {
      image = "[img]" + document.getElementsByClassName('image item selected')[0].getElementsByTagName('img')[0].src + "[/img]";
    }
  }

  if(document.getElementById('priceblock_ourprice') !== null) {
    price = ' (' + document.getElementById('priceblock_ourprice').innerHTML.trim() + ')';
  } else if(document.getElementById('priceblock_dealprice') !== null) {
    price = ' (' + document.getElementById('priceblock_dealprice').innerHTML.trim() + ')';
  } else if(document.getElementsByClassName('a-text-price') !== null && document.getElementsByClassName('a-text-price') !== undefined && document.getElementsByClassName('a-text-price').length > 0) {
    let aTextPrice = document.getElementsByClassName('a-text-price'),
        wasPrice = '',
        curPrice = '';
    for(let i = 0; i < aTextPrice.length; i++) {
      if(aTextPrice[i].hasAttribute('data-a-strike')) {
        wasPrice = '[s]' + aTextPrice[i].children[0].innerText + '[/s] ';
      }
      if(aTextPrice[i].classList.contains('apexPriceToPay')) {
        curPrice = aTextPrice[i].children[0].innerText;
      } else if(aTextPrice[i].classList.contains('a-size-medium')) {
        curPrice = aTextPrice[i].children[0].innerText;
      }
    }

    if(curPrice === '') {
      if(document.getElementsByClassName('priceToPay') !== null && document.getElementsByClassName('priceToPay').length === 1) {
        curPrice = document.getElementsByClassName('priceToPay')[0].querySelector('.a-offscreen').innerText
      }
    }

    price = ' (' + wasPrice + curPrice + ')';
  } else if(document.getElementsByClassName('a-offscreen')[0] !== null) {
    price = ' (' + document.getElementsByClassName('a-offscreen')[0].innerHTML.trim() + ')';
  }


  // else if(document.getElementsByClassName('apexPriceToPay')[0] !== undefined) {
  //   price = ' ([s]' + document.getElementsByClassName('a-price')[0].children[0].innerText + '[/s] ' + document.getElementsByClassName('apexPriceToPay')[0].children[0].innerText + ')';
  // } else if(document.getElementsByClassName('a-text-price')[0] !== undefined) {
  //   price = ' (' + document.getElementsByClassName('a-text-price')[0].children[0].innerText + ')';
  // }

  url = protocol + "//" + host + "/dp/" + asin + "/?tag=cl03f-20&smid=ATVPDKIKX0DER";
  result = "[url="+url+"]" + title + price + "[/url][br][br][url="+url+"]" + image + "[/url]";

  linkText.setAttribute('id','linkText');
  linkText.style.position = 'absolute';
  linkText.style.left = '-1000px';
  linkText.style.top = '-1000px';
  linkText.innerHTML = result;
  document.getElementsByTagName('body')[0].append(linkText);

  range.selectNode(document.getElementById('linkText'));
  selection = window.getSelection();
  selection.removeAllRanges();
  selection.addRange(range);

  try {
    success = document.execCommand('copy', false, null);
  } catch (e) {
    window.prompt("Press control-c",result);
  }
  if(success) {
    alert('Successfully copied to clipboard!');
  }

  document.getElementById('linkText').parentElement.removeChild(document.getElementById('linkText'));
}

function checkUrlChange() {
  if(curUrl !== location.href) {
    createButton();
    curUrl = location.href;
  }
}

function getQueryVariable(variable) {
  let query = window.location.search.substring(1),
      vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    let pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
  return(false);
}

function updateQueryStringParameter(uri, key, value) {
  let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i"),
      separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}
