function save_options() {
  let autoRedirect = document.getElementById('autoRedirect'),
      autoRedirectBool = false,
      redirectToSmile = document.getElementById('redirectToSmile'),
      redirectToSmileBool = false;
  if(autoRedirect.checked) {
    autoRedirectBool = true;
  }
  if(redirectToSmile.checked) {
    redirectToSmileBool = true;
  }
  chrome.storage.sync.set({
    autoRedirect: autoRedirectBool,
    redirectToSmile: redirectToSmileBool
  }, function() {
    let status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 1250);
  })
}

function restore_options() {
  chrome.storage.sync.get({
    autoRedirect: false,
    redirectToSmile: false
  }, function(items) {
    if(items.autoRedirect == true) {
      document.getElementById('autoRedirect').checked = true;
    }
    if(items.redirectToSmile == true) {
      document.getElementById('redirectToSmile').checked = true;
    }
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);